﻿using static Task_4.Matrix;
using static System.Console;

namespace Task_4
{
    internal class Program
    {
        static void Main()
        {
            SetWindowSize(30, 36);

            var array1 = new[,]
            {
                {1, 7, 3, 6},
                {4, 4, 5, 6},
                {7, 7, 5, 7},
                {7, 5, 6, 4}
            };

            var array2 = new[,]
            {
                {1, 6, 3, 6},
                {4, 3, 1, 3},
                {7, 7, 1, 3},
                {2, 5, 6, 1}
            };

            var matrixA = new Matrix(array1);
            var matrixB = new Matrix(array2);
            var martixC = matrixA + matrixB;
            var matrixD = matrixA - matrixB;
            var matrixE = matrixA * matrixB;
            var minorE0 = GetMinorOfElement(matrixA, 1, 1);
            var minorM0 = GetMinorOfMatrix(matrixA, 1, 1);

            WriteLine("Matrix A: \n" + matrixA);
            WriteLine("Matrix B: \n" + matrixB);
            WriteLine("Matrix C = A + B: \n" + martixC);
            WriteLine("Matrix D = A - B: \n" + matrixD);
            WriteLine("Matrix E = A * B: \n" + matrixE);
            WriteLine($"Minor of MatrixA: \n{minorM0}");
            ReadKey();
        }
    }
}
