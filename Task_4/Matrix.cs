﻿using System;

namespace Task_4
{
    public class Matrix
    {
        private int[,] matrix;

        public int this[int n, int m]
        {
            set { matrix[m, n] = value; }
            get { return matrix[n, m]; }
        }

        public Matrix(int[,] matrix)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException();
            }

            this.matrix = new int[matrix.GetLength(0), matrix.GetLength(1)];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    this.matrix[i, j] = matrix[i, j];
                }
            }
        }

        public Matrix(int n, int m)
        {
            matrix = new int[n, m];
        }

        public static Matrix operator +(Matrix left, Matrix right)
        {
            if (left.matrix.Length != right.matrix.Length)
            {
                throw new ArgumentException("Length of matrix is different");
            }

            if ((left == null) || (right == null))
            {
                throw new ArgumentNullException("Some argument is null");
            }

            var returnValue = new Matrix(left.matrix.GetLength(0), left.matrix.GetLength(1));

            for (int i = 0; i < returnValue.matrix.GetLength(0); i++)
            {
                for (int j = 0; j < returnValue.matrix.GetLength(1); j++)
                {
                    returnValue.matrix[i, j] = left[i, j] + right[i, j];
                }
            }

            return returnValue;
        }

        public static Matrix operator -(Matrix left, Matrix right)
        {
            if (left.matrix.Length != right.matrix.Length)
            {
                throw new ArgumentException("Length of matrix is different");
            }

            if ((left == null) || (right == null))
            {
                throw new ArgumentNullException("Some argument is null");
            }

            var returnValue = new Matrix(left.matrix.GetLength(0), left.matrix.GetLength(1));

            for (int i = 0; i < returnValue.matrix.GetLength(0); i++)
            {
                for (int j = 0; j < returnValue.matrix.GetLength(1); j++)
                {
                    returnValue.matrix[i, j] = left[i, j] - right[i, j];
                }
            }

            return returnValue;
        }

        public static Matrix operator *(Matrix left, Matrix right)
        {
            if (left.matrix.Length != right.matrix.Length)
            {
                throw new ArgumentException("Length of matrix is different");
            }

            if ((left == null) || (right == null))
            {
                throw new ArgumentNullException("Some argument is null");
            }

            var returnValue = new Matrix(left.matrix.GetLength(0), right.matrix.GetLength(1));

            for (int i = 0; i < left.matrix.GetLength(0); i++)
            {
                for (int j = 0; j < right.matrix.GetLength(1); j++)
                {
                    for (int k = 0; k < right.matrix.GetLength(0); k++)
                    {
                        returnValue.matrix[i, j] += left.matrix[i, k] * right.matrix[k, j];
                    }
                }
            }

            return returnValue;
        }

        public static int GetMinorOfElement(Matrix matrix, int row, int column)
        {
            var minor = GetMinorOfMatrix(matrix, row, column);
            return GetDeterm(minor);
        }

        public static Matrix GetMinorOfMatrix(Matrix matrix, int row, int column)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException();
            }

            var inerMatrix = matrix.matrix;

            if (inerMatrix.GetLength(0) != inerMatrix.GetLength(1))
            {
                throw new Exception("Wrong Matrix size");
            }

            var returnValue = new int[inerMatrix.GetLength(0) - 1, inerMatrix.GetLength(0) - 1];

            for (int i = 0; i < inerMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < inerMatrix.GetLength(1); j++)
                {
                    if ((i != row) || (j != column))
                    {
                        if (i > row && j < column) returnValue[i - 1, j] = inerMatrix[i, j];
                        if (i < row && j > column) returnValue[i, j - 1] = inerMatrix[i, j];
                        if (i > row && j > column) returnValue[i - 1, j - 1] = inerMatrix[i, j];
                        if (i < row && j < column) returnValue[i, j] = inerMatrix[i, j];
                    }
                }
            }

            return new Matrix(returnValue);
        }

        private static int GetDeterm(Matrix matrix)
        {
            var returnValue = 0;
            var inerMatrix = matrix.matrix;
            var Rank = inerMatrix.GetLength(0);

            if (matrix == null)
            {
                throw new ArgumentNullException();
            }

            if (inerMatrix.GetLength(0) != inerMatrix.GetLength(1))
            {
                throw new Exception("Wrong matrix size");
            }

            if (Rank == 1) returnValue = inerMatrix[0, 0];
            if (Rank == 2) returnValue = inerMatrix[0, 0] * inerMatrix[1, 1] - inerMatrix[0, 1] * inerMatrix[1, 0];

            if (Rank > 2)
            {
                for (int j = 0; j < inerMatrix.GetLength(1); j++)
                {
                    returnValue += (int)Math.Pow(-1, 0 + j) * inerMatrix[0, j] * GetDeterm(GetMinorOfMatrix(matrix, 0, j));
                }
            }

            return returnValue;
        }

        public override string ToString()
        {
            string returnValue = "";

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    returnValue += $"{matrix[i, j]}   "; 
                }

                returnValue += "\n";
            }

            return returnValue;
        }
    }
}
